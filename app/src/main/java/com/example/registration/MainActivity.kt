package com.example.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    private lateinit var regtextview : TextView
    private lateinit var emailedit : EditText
    private lateinit var passedit : EditText
    private lateinit var secpassedit : EditText
    private lateinit var button : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    text()
    registerlistener()
    }
    private fun text(){
        regtextview = findViewById(R.id.textView)
        emailedit = findViewById(R.id.editTextEmail)
        passedit = findViewById(R.id.editTexPassword)
        secpassedit = findViewById(R.id.editTextPassword2)
        button = findViewById(R.id.button)
    }
    private fun registerlistener(){
        button.setOnClickListener{
            val email = emailedit.text.toString()
            val password = passedit.text.toString()
            val password2 = secpassedit.text.toString()
            
            if (email.isEmpty() || password.isEmpty() || password2.isEmpty()) {
                Toast.makeText(this, "ველი ცარიელია", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            if (password == password2){
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener{akj ->
                        if (akj.isSuccessful){
                            startActivity(Intent(this,profileactivity::class.java))

                        }else{
                            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                        }

                    }




            }else{
                Toast.makeText(this, "password does not match", Toast.LENGTH_SHORT).show()
            }


        }


    }
}